import _regeneratorRuntime from "babel-runtime/regenerator";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var send = function () {
	var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee() {
		var overlay, url, name, email, text, data, responce;
		return _regeneratorRuntime.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						overlay = document.getElementById('load-overlay');

						//Адрес формы на formcarry

						url = 'https://formcarry.com/s/Q2zFSovOxpX';
						name = document.getElementById('form-id').value;


						if (!name) {
							name = localStorage.getItem('name');
							document.getElementById('form-id').value = name;
						} else {
							localStorage.setItem('name', name);
						}

						email = document.getElementById('form-email').value;


						if (!email) {
							email = localStorage.getItem('mail');
							document.getElementById('form-email').value = email;
						} else {
							localStorage.setItem('mail', email);
						}

						text = document.getElementById('form-text').value;


						if (!text) {
							text = localStorage.getItem('text');
							document.getElementById('form-text').value = text;
						} else {
							localStorage.setItem('text', text);
						}

						data = { name: name, email: email, comment: text };


						overlay.style.display = "inline";

						_context.prev = 10;
						_context.next = 13;
						return fetch(url, {
							method: 'POST',
							headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
							body: JSON.stringify(data)
						});

					case 13:
						responce = _context.sent;


						if (responce.ok) {
							alert("Thanks for message! We will contact you soon.");
							localStorage.clear();

							document.getElementById('form-id').value = "";
							document.getElementById('form-email').value = "";
							document.getElementById('form-text').value = "";
						} else {
							alert("Error " + responce.status);
						}
						_context.next = 20;
						break;

					case 17:
						_context.prev = 17;
						_context.t0 = _context["catch"](10);

						alert("Error" + _context.t0);

					case 20:

						overlay.style.display = "none";

					case 21:
					case "end":
						return _context.stop();
				}
			}
		}, _callee, this, [[10, 17]]);
	}));

	return function send() {
		return _ref.apply(this, arguments);
	};
}();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Form = function (_React$Component) {
	_inherits(Form, _React$Component);

	function Form() {
		_classCallCheck(this, Form);

		return _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).apply(this, arguments));
	}

	_createClass(Form, [{
		key: "render",
		value: function render() {
			return React.createElement(
				"div",
				null,
				React.createElement(
					"p",
					null,
					React.createElement(
						"strong",
						null,
						"\u0421\u0423\u041F\u0415\u0420 \u041F\u0423\u041F\u0415\u0420 \u0422\u0415\u041B\u0415\u0412\u0418\u0417\u041E\u0420\u042B"
					)
				),
				React.createElement(
					"p",
					null,
					"+86(186)66660854"
				),
				React.createElement(
					"p",
					null,
					"E-mail: ",
					React.createElement(
						"a",
						{ href: "" },
						"sales@aviselectronics.com"
					)
				),
				React.createElement(
					"p",
					null,
					"Mon-Fri: 09:00 - 18:00,",
					React.createElement("br", null),
					"Sat: 09:00 - 12:00(UTC +08:00)"
				),
				React.createElement(
					"form",
					{ "accept-charset": "UTF-8" },
					React.createElement("input", { type: "text", placeholder: "Name", name: "name", id: "form-id" }),
					React.createElement("br", null),
					React.createElement("input", { type: "email", placeholder: "E-mail", name: "e-mail", id: "form-email" }),
					React.createElement("br", null),
					React.createElement("textarea", { placeholder: "Comment", id: "form-text" }),
					React.createElement("br", null),
					React.createElement(
						"button",
						{ type: "button", "class": "submit", id: "form-send", onClick: send },
						"SEND"
					)
				)
			);
		}
	}]);

	return Form;
}(React.Component);

ReactDOM.render(React.createElement(Form, null), document.getElementById('contact'));