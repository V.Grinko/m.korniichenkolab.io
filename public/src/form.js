class form extends React.Component {
    render() {
        return (
            <div>
			    <p><strong>СУПЕР ПУПЕР ТЕЛЕВИЗОРЫ</strong></p>
			    <p>+86(186)66660854</p>
			    <p>E-mail: <a href="">sales@aviselectronics.com</a></p>
			    <p>Mon-Fri: 09:00 - 18:00,<br />Sat: 09:00 - 12:00(UTC +08:00)</p>
			    <form accept-charset="UTF-8">
				    <input type="text" placeholder="Name" name="name" id="form-id" />
				    <br />
				    <input type="email" placeholder="E-mail" name="e-mail" id="form-email" />
				    <br />
				    <textarea placeholder="Comment" id="form-text"></textarea>
				    <br />
				    <button type="button" class="submit" id="form-send" onClick={send}>SEND</button>
			    </form>
		    </div>
        );
    }
}

async function send()
{
	let overlay = document.getElementById('load-overlay');

	//Адрес формы на formcarry
	let url = 'https://formcarry.com/s/Q2zFSovOxpX';

	let name = document.getElementById('form-id').value;

	if (!name)
	{
		name = localStorage.getItem('name');
		document.getElementById('form-id').value = name;
	}
	else
	{
		localStorage.setItem('name', name);
	}

	let email = document.getElementById('form-email').value;
		
	if (!email)
	{
		email = localStorage.getItem('mail');
		document.getElementById('form-email').value = email;
	}
	else
	{
		localStorage.setItem('mail', email);
	}

	let text = document.getElementById('form-text').value;

	if (!text)
	{
		text = localStorage.getItem('text');
		document.getElementById('form-text').value = text;
	}
	else
	{
		localStorage.setItem('text', text);
	}

    const data = {name : name, email : email, comment : text};
        
	overlay.style.display = "inline";

	try {

	const responce = await fetch(url, {
            method: 'POST',
			headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
			body: JSON.stringify(data)
	});

	if (responce.ok)
	{
		alert("Thanks for message! We will contact you soon.");
		localStorage.clear();

		document.getElementById('form-id').value = "";
		document.getElementById('form-email').value = "";
		document.getElementById('form-text').value = "";
	}
	else
	{
		alert("Error " + responce.status);
	}
	}
	catch(e)
	{
		alert("Error" + e);
	}
		
	overlay.style.display = "none";
}

ReactDOM.render(
	<Form />,
	document.getElementById('contact')
);