function quad(timeFraction)
{
    return Math.pow(timeFraction, 2);
}

var scrolled = false;

function easeOut(timing)
{
    return function(timeFraction) {
        return 1 - timing(1 - timeFraction);
    };
}

let quadEaseOut = easeOut(quad);

function animate({timing, draw, duration}) {

    let start = performance.now();
  
    requestAnimationFrame(function animate(time) {
      // timeFraction изменяется от 0 до 1
      let timeFraction = (time - start) / duration;
      if (timeFraction > 1) timeFraction = 1;
  
      // вычисление текущего состояния анимации
      let progress = timing(timeFraction);
  
      draw(progress); // отрисовать её
  
      if (timeFraction < 1) {
        requestAnimationFrame(animate);
      }
  
    });
}

function isVisible(elem) {

    let coords = elem.getBoundingClientRect();

    let windowHeight = document.documentElement.clientHeight;
    // видны верхний ИЛИ нижний край элемента
    let topVisible = coords.top < windowHeight && coords.top > 95;
    let bottomVisible = coords.bottom < windowHeight && coords.top < 95;

    return topVisible || bottomVisible;
}

function showContact(element) {

        animate({
            timing: quadEaseOut,
            draw(progress){
                element.style.left = 100 * (1.15 * progress - 1) + '%';
                element.style.opacity = progress;
            },
            duration: 2000
        });
}

document.addEventListener("DOMContentLoaded", function() { 

    let elem = document.getElementById('contact');
    if (isVisible(elem) && !scrolled)
    {
        showContact(elem);
        scrolled = true;
    }

    window.addEventListener('scroll', function () {

        let elem = document.getElementById('contact');
        if (isVisible(elem) && !scrolled)
        {
            showContact(elem);
            scrolled = true;
        }
    });
});
